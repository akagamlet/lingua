/**
 *  webpack config
 */

const ImageminPlugin = require('imagemin-webpack-plugin').default;
const imageminMozjpeg = require('imagemin-mozjpeg');
const merge = require('webpack-merge');
const ScriptExtHtmlWebpackPlugin = require('script-ext-html-webpack-plugin');
const HtmlWebpackInlineSVGPlugin = require('html-webpack-inline-svg-plugin');
const baseConfig = require('./webpack.base.config');

const {
    config
} = require('./config');

module.exports = (env, argv) => {
    const jsLoaders = [...[config[argv.mode].customOptions.useBabel === true ? {
        loader: 'babel-loader',
        options: {
            plugins: ['syntax-dynamic-import']
        }
    } : undefined], ...[config[argv.mode].customOptions.useEslint === true ? {
        loader: 'eslint-loader',
        options: {
            cache: false
        }
    } : undefined]].filter(item => item); // пустые слоты -- в топку

    const {
        styleLoader
    } = config[argv.mode].customOptions;

    delete config[argv.mode].customOptions;

    return merge(baseConfig, {
        ...config[argv.mode],
        mode: argv.mode !== 'production' ? 'development' : argv.mode,
        module: {
            rules: [jsLoaders.length ? {
                test: /\.m?js$/,
                exclude: /(node_modules|bower_components)/,
                use: jsLoaders
            } : {}].concat([{
                test: /\.less$/,
                exclude: /(node_modules|bower_components)/,
                use: [{
                        loader: styleLoader,
                        ...(argv.mode === 'production' ? {
                            options: {
                                publicPath: '../'
                            }
                        } : {})
                    }, {
                        loader: 'css-loader',
                        options: {
                            sourceMap: false
                        }
                    },
                    {
                        loader: 'postcss-loader'
                    },
                    {
                        loader: 'less-loader',
                        options: {
                            sourceMap: false
                        }
                    }
                ]
            }])
        }
    }, {
        plugins: [
            ...(argv.mode === 'production' ? [
                new ImageminPlugin({
                    disable: argv.mode === 'production',
                    pngquant: {
                        quality: '84'
                    },
                    plugins: [
                        imageminMozjpeg({
                            quality: 84,
                            progressive: true
                        })
                    ]
                })
            ] : []),
            new ScriptExtHtmlWebpackPlugin({
                defaultAttribute: 'defer'
            }),
            new HtmlWebpackInlineSVGPlugin({
                runPreEmit: true // путь inline svg теперь будет отсчитываться от package.json
            })
        ]
    });
};
