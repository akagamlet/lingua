import './less/common/index.less';
import './less/pages/index.less';

import 'classlist-polyfill';
import 'polifills';
import reviewSlider from 'components/reviewSlider';
import Glide from '@glidejs/glide/';
import 'helpers/lazy';

document.addEventListener('DOMContentLoaded', () => {
    /**
     *  review slider
     */
    requestAnimationFrame(() => {
        reviewSlider.mount();
    });

    /**
     *  coorporate slider
     */

    const corporateSlider = new Glide('.glide.glide--coorporate', {
        type: 'carousel',
        startAt: 0,
        perView: 1,
        peek: 0,
        gap: 0,
        dragThreshold: true
    });

    requestAnimationFrame(() => {
        corporateSlider.mount();
    });

    requestAnimationFrame(async () => {
        try {
            const {
                default: stickyHeader
            } = await import(/* webpackChunkName: "stickyHeader", webpackMode: "lazy" */ 'components/stickyHeader');
            stickyHeader();
        } catch (error) {
            console.log(error.message);
        }
    });
});
