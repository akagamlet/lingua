
import './less/common/index.less';
import './less/pages/obuchenie.less';

import 'classlist-polyfill';
import 'polifills';
import 'helpers/lazy';

document.addEventListener('DOMContentLoaded', () => {
    requestAnimationFrame(async () => {
        try {
            const {
                default: stickyHeader
            } = await import(/* webpackChunkName: "stickyHeader", webpackMode: "lazy" */ 'components/stickyHeader');
            stickyHeader();
        } catch (error) {
            console.log(error.message);
        }
    });
});
