const throwError = (message) => {
    console.log('error', message);
};

/**
 *
 * получаем Node по селектору
 * @param selector - селектор элемента (.some etc)
 * @param parent - если нужно указываем родителя
 * @returns {*}
 */

function getNode(selector, parent) {
    if (!selector) {
        return false;
    }

    const targetNode = parent || document;
    const node = targetNode.querySelector(selector);

    if (!node) {
        throwError(`${selector} not found in document.`);
        return false;
    }

    return node;
}

/**
 *
 * получаем массив Node по селектору
 * @param selector - селектор элемента (.some etc)
 * @param parent - если нужно указываем родителя
 * @returns {*}
 */


function getNodes(selector, parent) {
    if (!selector) {
        return false;
    }

    const targetNode = parent || document;
    const nodes = [...targetNode.querySelectorAll(selector)];

    if (!nodes) {
        throwError(`${selector} not found in document.`);
        return false;
    }

    return nodes;
}

export {
    getNode,
    getNodes
};
