(function (ElementProto) {

    if (typeof ElementProto.matches !== 'function') {

        ElementProto.matches = ElementProto.msMatchesSelector || ElementProto.mozMatchesSelector || ElementProto.webkitMatchesSelector || function matches(selector) {
            var element = this;
            var elements = (element.document || element.ownerDocument).querySelectorAll(selector);
            var index = 0;

            while (elements[index] && elements[index] !== element) {
                ++index;
            }

            return Boolean(elements[index]);
        };
    }

    if (typeof ElementProto.closest !== 'function') {

        try {

            ElementProto.closest = function closest(selector) {

                var element = this;

                while (element) {

                    if (element.matches(selector)) {
                        return element;
                    } else {
                        element = (element.tagName === 'svg' || (typeof SVGElementInstance !== 'undefined' && element instanceof SVGElementInstance)) ? element.parentNode : element.parentElement;
                    }
                }

                return null;

            };

        } catch (e) {
            console.log(e);
            return null;
        }
    }
})(window.Element.prototype);