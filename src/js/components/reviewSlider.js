/**
 *  reviews
 *  @TODO --- отрефакторить . сделать нормальный класс - фабрику -  а лучше декоратор слайдера
 */


import {
    getNodes,
    getNode
} from 'helpers/dom';
import Glide from '@glidejs/glide/';

const reviewSliderSlidesNodes = getNodes('.reviews .glide__slide:not(.glide__slide--clone) img');
let reviewSliderLeftPhoto = null;
let reviewSliderRightPhoto = null;

const reviewSlider = new Glide('.glide.glide--review', {
    type: 'carousel',
    startAt: 0,
    perView: 1,
    peek: 0,
    gap: 0,
    dragThreshold: true
});

/**
 * @param number currentSliderIndex
 * @return {leftIndex, rightIndex}
 */

const calculateNextPrevImageIndex = (currentSliderIndex = 0) => {
    if (!reviewSliderSlidesNodes.length) {
        throw new Error('review slides not present in dom');
    }
    const rightIndex = typeof reviewSliderSlidesNodes[currentSliderIndex + 1] === 'undefined' ? 0 : currentSliderIndex + 1;
    const leftIndex = typeof reviewSliderSlidesNodes[currentSliderIndex - 1] === 'undefined' ? reviewSliderSlidesNodes.length - 1 : currentSliderIndex - 1;
    return {
        rightIndex,
        leftIndex
    };
};

/**
 * @param {Glide} slider
 */

const updateNextPrevImage = (slider) => {
    if (!(slider instanceof Glide)) {
        throw new Error('slider must be a Glide type');
    }

    if (!(Array.isArray(reviewSliderSlidesNodes) && reviewSliderSlidesNodes.length)) {
        return;
    }

    if (slider.isType('carousel') && reviewSliderLeftPhoto && reviewSliderRightPhoto) {
        try {
            const {
                leftIndex,
                rightIndex
            } = calculateNextPrevImageIndex(slider.index);
            reviewSliderLeftPhoto.setAttribute('src', reviewSliderSlidesNodes[leftIndex].getAttribute('data-src'));
            reviewSliderRightPhoto.setAttribute('src', reviewSliderSlidesNodes[rightIndex].getAttribute('data-src'));
        } catch (e) {
            console.log(e.message);
        }
    }
};

reviewSlider.on('mount.before', () => {
    const reviewsNode = getNode('.reviews');
    if (!reviewsNode) {
        return;
    }
    reviewsNode.insertAdjacentHTML('afterbegin', `
            <img src="" class="reviews__first-item" alt=""f />
            <img src="" class="reviews__last-item" alt="" />
        `);

    reviewSliderLeftPhoto = getNode('.reviews__first-item');
    reviewSliderRightPhoto = getNode('.reviews__last-item');

    updateNextPrevImage(reviewSlider);
});

reviewSlider.on('run', () => {
    updateNextPrevImage(reviewSlider);
});

//

export default reviewSlider;
