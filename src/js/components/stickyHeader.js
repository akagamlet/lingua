import Headroom from 'headroom.js';
import {
    getNode
} from 'helpers/dom';
// @TODO -- отрефакторить
export default function stickyHeader(selector = '.sticky-header') {
    const stickyNode = getNode(selector);
    if (!stickyNode) {
        throw new Error('sticky element was not found');
    }
    const headroom = new Headroom(stickyNode, {
        offset: 131,
        tolerance: {
            up: 170,
            down: 0
        },
        classes: {
            initial: 'animated',
            pinned: 'slideDown',
            unpinned: 'slideUp'
        }
    });
    headroom.init();
}
