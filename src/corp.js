import './less/common/index.less';
import './less/pages/corp.less';

import 'classlist-polyfill';
import 'polifills';
import 'helpers/lazy';
import Glide from '@glidejs/glide/';

document.addEventListener('DOMContentLoaded', () => {
    requestAnimationFrame(async () => {
        try {
            const {
                default: stickyHeader
            } = await import(/* webpackChunkName: "stickyHeader", webpackMode: "lazy" */ 'components/stickyHeader');
            stickyHeader();
        } catch (error) {
            console.log(error.message);
        }
    });
    /**
     *  coorporate slider
     */

    const learningSlider = new Glide('.glide.glide--learning', {
        type: 'carousel',
        startAt: 0,
        perView: 4,
        peek: 0,
        gap: 40,
        dragThreshold: true,
        breakpoints: {
            768: {
                perView: 3
            },
            650: {
                perView: 2
            },
            500: {
                perView: 1
            }
        }
    });

    requestAnimationFrame(() => {
        learningSlider.mount();
    });
});
