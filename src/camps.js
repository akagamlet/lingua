import './less/common/index.less';
import './less/pages/camps.less';

import 'classlist-polyfill';
import 'polifills';
import {
    getNode
} from 'helpers/dom';
import Glide from '@glidejs/glide/';
import reviewSlider from 'components/reviewSlider';
import 'helpers/lazy';


/**
 * подключаем обработчик клика и загружаем галерею картинок (асинхронно)
 * @param {Glide} slider
 * @param string selector
 * TODO -- сделать интерфейс более абстрактным -- для других страниц
 */

const initImgGallery = ({
    slider = null,
    selector = document,
    startIndex = 0
} = {}) => {
    if (slider !== null && !(slider instanceof Glide)) {
        throw new Error('slider must be a Glide type');
    }
    let gallery = null;
    let baguetteBox = null;
    getNode('.carousel__camps-show-all').addEventListener('click', async (event) => {
        event.preventDefault();
        event.stopImmediatePropagation();
        if (!baguetteBox) {
            const {
                default: module
            } = await import(/* webpackChunkName: "baguetteBoxImg", webpackMode: "lazy" */ 'baguettebox.js');
            baguetteBox = module;
        }
        if (!gallery) {
            try {
                gallery = baguetteBox.run(selector, {
                    fullScreen: true,
                    noScrollbars: true,
                    bodyClass: ''
                });
            } catch (e) {
                throw e; // пробрасываем дальше
            }
        }
        if (slider) {
            // стили подгружаем тоже динамически... ибо вдруг ниикто не кликнет на фотки....
            await import(/* webpackChunkName: "baguetteBoxCss", webpackMode: "lazy" */ 'baguettebox.js/dist/baguetteBox.min.css');
            return baguetteBox.show(slider.index + 2, gallery[0]);
        }

        return baguetteBox.show(startIndex, gallery[0]);
    });
};

document.addEventListener('DOMContentLoaded', () => {
    if (getNode('.glide.carousel--camps')) {
        // @TODO сделать сборку слайдера при достижении скрола, а лучше вынести отдельно в chunk
        const campSlider = new Glide('.glide.carousel--camps', {
            type: 'carousel',
            startAt: 0,
            perView: 1,
            peek: 470,
            gap: 0,
            breakpoints: {
                1750: {
                    peek: 350
                },
                1500: {
                    peek: 200
                },
                1200: {
                    peek: 150
                },
                1100: {
                    peek: 0
                }
            }
        });

        // // отложим монтирование слайдера
        requestAnimationFrame(() => {
            try {
                initImgGallery({
                    slider: campSlider,
                    selector: '.js-camp-gallery'
                });
                campSlider.mount();
            } catch (e) {
                console.log(e.message);
            }
        });

        /**
         *  review slider
         */

        requestAnimationFrame(() => {
            reviewSlider.mount();
        });
    }

    // липкий header
    requestAnimationFrame(async () => {
        try {
            const {
                default: stickyHeader
            } = await import(/* webpackChunkName: "stickyHeader", webpackMode: "lazy" */ 'components/stickyHeader');
            stickyHeader();
        } catch (error) {
            console.log(error.message);
        }
    });

    // @TODO -- асинхронно подгружать сам слайдер при при попадании по вьюпорт
    // requestAnimationFrame(async () => {
    //     const {
    //         default: Glide
    //     } = await import(/* webpackChunkName: "glideSlider" */ '@glidejs/glide');
    //     new Glide('.glide', {
    //         type: 'carousel',
    //         startAt: 0,
    //         perView: 1,
    //         peek: 470
    //     }).mount();
    // });
});
