/**
 *  config for env
 */

const path = require('path');
const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const PreloadWebpackPlugin = require('preload-webpack-plugin');

/**
 *  среду выполения брать из Node.env и пилить сюда -- а лучше в webpack.base там вставлять
 */

module.exports = {
    config: {
        // dev
        development: {
            customOptions: {
                useEslint: true, // возможно вынести в package.json отдельной таской
                useBabel: true,
                cssSourceMap: true,
                styleLoader: 'style-loader'
            },
            entry: {
                index: './src/obuchenie.js',
            },
            plugins: [
                new webpack.DefinePlugin({
                    DEV_MODE: JSON.stringify('true')
                }),
                new HtmlWebpackPlugin({
                    template: './src/templates/pages/obuchenie.pug',
                    inject: 'body',
                    hash: true,
                    meta: {},
                    filename: 'index.html',
                })
            ],
            // devtool: 'inline-source-map',
            devServer: {
                contentBase: path.join(__dirname, 'dist'),
                compress: true,
                port: 8080,
                clientLogLevel: 'warning',
                historyApiFallback: false,
                hot: false,
                https: false,
                inline: true,
                overlay: true,
                watchContentBase: false,
                writeToDisk: false
            },
            optimization: {
                minimize: false
            }
        },
        // production
        production: {
            customOptions: {
                useEslint: true,
                useBabel: true,
                styleLoader: MiniCssExtractPlugin.loader
            },
            entry: {
                index: './src/index.js',
                cource: './src/cource.js',
                obuchenie: './src/obuchenie.js',
                camps: './src/camps.js',
                corp: './src/corp.js',
            },
            plugins: [
                new CleanWebpackPlugin(),
                new webpack.DefinePlugin({
                    DEV_MODE: JSON.stringify('false')
                }),
                new HtmlWebpackPlugin({
                    template: './src/templates/pages/index.pug',
                    inject: 'body',
                    hash: true,
                    meta: {},
                    filename: 'index.html',
                    chunks: ['index', 'common'],
                }),
                new HtmlWebpackPlugin({
                    template: './src/templates/pages/cource.pug',
                    inject: 'body',
                    hash: true,
                    meta: {},
                    filename: 'cource.html',
                    chunks: ['cource', 'common']
                }),
                new HtmlWebpackPlugin({
                    template: './src/templates/pages/obuchenie.pug',
                    inject: 'body',
                    hash: true,
                    filename: 'obuchenie.html',
                    chunks: ['obuchenie', 'common'],
                    meta: {}
                }),
                new HtmlWebpackPlugin({
                    template: './src/templates/pages/camps.pug',
                    inject: 'body',
                    hash: true,
                    meta: {},
                    filename: 'camps.html',
                    chunks: ['camps', 'common', 'baguetteBoxImg'],
                }),
                new HtmlWebpackPlugin({
                    template: './src/templates/pages/corp.pug',
                    inject: 'body',
                    hash: true,
                    meta: {},
                    filename: 'corp.html',
                    chunks: ['corp', 'common'],
                }),
                new MiniCssExtractPlugin({
                    filename: 'css/[name].css',
                    chunkFilename: 'css/[name].css',
                }),
                new PreloadWebpackPlugin({
                    rel: 'preload',
                    as(entry) {
                        if (/\.css$/.test(entry)) return 'style';
                        if (/\.woff2$/.test(entry)) return 'font';
                        return 'script';
                    },
                    fileWhitelist: [/\.woff2/],
                    include: 'allAssets'
                }),
            ],
            optimization: {
                splitChunks: {
                    chunks: 'initial',
                    name: 'common'
                }
            }
        }
    }
};
