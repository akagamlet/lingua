/**
 *  глобыльные настройки проекта.
 *  @TODO - возможно для каждого окружения вынести параметры в отдельный конфиг .....
 */


module.exports = {
    srcPath: 'src',
    assets: 'assets',
    imgPath: 'img',
    lessFolder: 'less'
};
