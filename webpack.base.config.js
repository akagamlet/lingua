/**
 *  webpack base config
 */

const path = require('path');
const globalOptions = require('./config/globals');

module.exports = {
    module: {
        rules: [{
                test: /\.pug$/,
                use: [{
                    loader: 'pug-loader',
                    options: {
                        pretty: true
                    }
                }]
            }, {
                test: /\.txt$/,
                exclude: /(node_modules|bower_components)/,
                use: 'raw-loader'
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        outputPath: 'fonts/',
                        publicPath: '../fonts/'
                    }
                }]
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                use: [{
                    loader: 'url-loader',
                    options: {
                        outputPath: `${globalOptions.imgPath}`,
                        publicPath: '',
                        name: '[name][hash].[ext]',
                        limit: 8092
                    }
                }]
            },
            {
                test: /.css$/,
                use: [
                    {
                        loader: 'style-loader'
                    },
                    {
                        loader: 'css-loader',
                        options: {
                        }
                    }
                ]
            }
        ]
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'js/[name].bundle.js',
        chunkFilename: 'js/chunks/[name].bundle.js',
        publicPath: '/',
    },
    resolve: {
        modules: [
            'node_modules',
            path.resolve(__dirname, `${globalOptions.srcPath}/${globalOptions.assets}/${globalOptions.imgPath}/tmp/`),
            path.resolve(__dirname, `${globalOptions.srcPath}/${globalOptions.assets}/${globalOptions.imgPath}/bgs/`),
            path.resolve(__dirname, `${globalOptions.srcPath}/${globalOptions.assets}/${globalOptions.imgPath}/`),
        ],
        alias: {
            '@': path.resolve(__dirname, `${globalOptions.srcPath}`),
            helpers: path.resolve(__dirname, `${globalOptions.srcPath}/js/helpers/`),
            components: path.resolve(__dirname, `${globalOptions.srcPath}/js/components/`),
            polifills: path.resolve(__dirname, `${globalOptions.srcPath}/js/polifills/`),
            img: path.resolve(__dirname, `${globalOptions.srcPath}/${globalOptions.assets}/${globalOptions.imgPath}/`)
        }
    },
    plugins: []
};
