const mode = 'production'; // брать из webpack cfg

const presets = [
    [
        '@babel/env',
        {
            useBuiltIns: 'usage',
            debug: true,
            loose: false,
            modules: false,
            targets: mode === 'development' ? 'last 2 Chrome versions' : 'last 40 version, not ie <= 9'
        },
    ],
];

module.exports = {
    presets
};
