const path = require('path');
const globalOptions = require('./config/globals');

module.exports = {
    plugins: (() => [
        require('autoprefixer'),
        require('cssnano'),
        require('postcss-flexbugs-fixes'),
        require('postcss-inline-svg')({
            path: path.resolve(__dirname, `${globalOptions.srcPath}/${globalOptions.assets}/${globalOptions.imgPath}/svg/`)
        }),
        require('postcss-assets')({
            loadPaths: [
                path.resolve(__dirname, `${globalOptions.srcPath}/${globalOptions.assets}/${globalOptions.imgPath}/`),
                path.resolve(__dirname, `${globalOptions.srcPath}/${globalOptions.assets}/${globalOptions.imgPath}/svg/`)
            ]
        }),
    ])()
};
