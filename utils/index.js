/**
 *  helper for create html files with HtmlWebpckPlugin
 */

const HtmlWebpackPlugin = require('html-webpack-plugin');
const envConfig = require('../config/index.js');

module.exports.createHtml = (mode) => {
    if (!envConfig[mode]) {
        return [];
    }
    return (envConfig[mode]).pages.map((templateName) => {
        const {
            filename,
            inlineSource,
            chunks = [],
            inject = 'body',
            hash = false,
            minify = false
        } = templateName;
        return new HtmlWebpackPlugin({
            inject,
            minify,
            hash,
            filename,
            template: `src/templates/pages/${templateName.template}`,
            inlineSource,
            chunks
        });
    });
};


/**
 *  make asset
 */

module.exports.assetsPath = (subDirectoryPath) => {
    const asssetSubDirectory = subDirectoryPath;
    return asssetSubDirectory;
};
