/**
 *  хелпер для создания БЕМ блоков
 */

const fs = require('fs');
const mkdirp = require('mkdirp');
const path = require('path');
const messages = require('./messages');

const currentConfig = require('./../config/globals');

function fileExists(_path) {
    try {
        fs.statSync(_path);
    } catch (err) {
        return !(err && err.code === 'ENOENT');
    }
    return true;
}

// @ name of created bem block

const blockName = process.argv[2];
const blockNameSpace = process.argv[3] === 'null';

if (blockNameSpace === true) {
    console.log(' не генерировать в common less ');
}


// @ default file extension for bem blocks
// const defaultExtensions = ['less', 'img'];

const defaultExtensions = ['less'];

/**
 *  настройки путей etc @TODO -- переделать
 */

const blockScope = blockNameSpace === false ? 'common' : '';
const lessFolderPath = path.resolve(__dirname, `../${currentConfig.srcPath}`);
const newBlockDirPath = path.resolve(lessFolderPath, path.join('less', 'blocks', `${blockScope}`, `${blockName}`));
const pathToWriteToLessFile = path.resolve(lessFolderPath, path.join('less', 'blocks', `${blockScope}`, 'index.less'));

if (blockName) {
    if (fs.existsSync(newBlockDirPath) === true) {
        console.error(messages.blockExists);
        return false;
    }

    mkdirp(newBlockDirPath, (err) => {
        if (err) {
            console.error(`${messages.cancel} ${err}`);
        } else {
            console.log(`${messages.createFolder} ${newBlockDirPath} `);
            defaultExtensions.forEach((oneExt) => {
                // @full path to file
                const filePath = path.join(`${newBlockDirPath}`, `${blockName}.${oneExt}`);
                const fileContent = `/*** \n\n */ \n\n.${blockName} {\n}`;
                const fileImportContent = `@import (less) "./${blockName}/${blockName}.less";\n`;
                // create less
                if (oneExt === 'less') {
                    if (fileExists(filePath) === false) {
                        fs.writeFile(filePath, fileContent, (err) => {
                            if (err) {
                                return console.log(`${messages.errorCreateFile} ${err}`);
                            }
                            if (blockScope) {
                                fs.appendFileSync(pathToWriteToLessFile, fileImportContent);
                            }
                            return console.log(`${messages.fileCreated} ${filePath}`);
                        });
                    }
                }
            });
        }
    });
}
